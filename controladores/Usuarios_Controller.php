<?php
    include("./entidades/Usuario.php");
session_start();
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Usuarios_Controller
 *
 * @author pabhoz
 */
 class Usuarios_Controller {

     public function login(){
         $username = $_POST["username"];
         $password = $_POST["password"];
         // $email = $_POST["email"];

         $usr = new Usuario(0, $username, $password);
         $r = $usr->login();

         $r = ($r) ? ["error"=>0,"msg"=>"Bienvenido $username"] : ["error"=>1,"msg"=>"El "
             . "nombre de usuario o la contraseña es incorrecta"];
         print json_encode($r);


      // return $usr;
     }




    public function register(){
    $usr = new Usuario(null, $_POST["username"], $_POST["password"],$_POST["email"],
            $_POST["genero"], $_POST["lat"], $_POST["lon"], 100,'Solter@','Sin descripcion','Cali', $_POST["edad"],30,null);
    $r = $usr->save();
    $intento= $usr->cargarPerfil();
    //var_dump($r);
    $respuesta = ($r) ? ["error"=>0,"msg"=>"Usuario ".$_POST['username']." creado correctamente"]
            : ["error"=>1,"msg"=>"Error al crear usuario"];
    print json_encode($intento);
}


    public function crearSession(){
      $username = $_POST["username"];
      $password = $_POST["password"];
      session_start();
      $_SESSION['persona']  = $username;
      $_SESSION['contraseña'] = $password;
      // $respose = $perfil->cargarPerfil();
      $res=new Usuario(0,$_SESSION['persona'] ,$_SESSION['contraseña']);
      $respose = true;
      print json_encode($respose);

    }
    public function cargarPerfil(){
      // session_start();
      $usr = new Usuario(0,$_SESSION['persona'] ,$_SESSION['contraseña']);
      $res = $usr->cargarPerfil();
      foreach ($res as $usuario) {
        $_SESSION['id'] = $usuario["id"];
      }
      // var_dump($res);
      return $res;
    }

        public function mostrarUsuarios(){
          // session_start();
          $usr = new Usuario(0,$_SESSION['persona'] ,$_SESSION['contraseña']);
          $perfil= $usr->cargarPerfil();
          foreach ($perfil as $usuario) {
            $obj = new Usuario($usuario["id"],$usuario["username"] ,$usuario["password"],$usuario["email"],$usuario["genero"],$usuario["lat"],$usuario["lon"],$usuario["radio"],$usuario["estado"],$usuario["descripcion"],$usuario["ciudad"],
            $usuario["edad"],$usuario["rangoEdad"],$usuario["imgProfile"]);
          }

          $res = $obj->mostrarUsuarios();
          return $res;
        }

        public function editarMe(){
          $edad = $_POST["edad"];
          $descripcion = $_POST["descripcion"];
          $estado = $_POST["estado"];
          // session_start();
          $usr = new Usuario(0,$_SESSION['persona'] ,$_SESSION['contraseña']);
          $res = $usr->cargarPerfil();
          foreach($res as $usuario){
            if($edad==""):$edad=$usuario["edad"]; endif;
            if($descripcion==""):$descripcion=$usuario["descripcion"]; endif;
            if($estado==""):$estado=$usuario["estado"]; endif;
            $obj = new Usuario($usuario["id"],$usuario["username"] ,$usuario["password"],$usuario["email"],$usuario["genero"],$usuario["lat"],$usuario["lon"],$usuario["radio"],$estado,$descripcion,$usuario["ciudad"],
            $edad,$usuario["rangoEdad"],$usuario["imgProfile"]);
          }
            $res = $obj->editarMe();

            print json_encode($res);

        }

        public function editarBusqueda(){
          $radio = $_POST["radio"];
          $rangoEdad = $_POST["rangoEdad"];
          $genero = $_POST["genero"];
          // session_start();
          $usr = new Usuario(0,$_SESSION['persona'] ,$_SESSION['contraseña']);
          $res = $usr->cargarPerfil();
          foreach($res as $usuario){
            if($radio==0):$radio=$usuario["radio"]; endif;
            if($rangoEdad==0):$rangoEdad=$usuario["rangoEdad"]; endif;
            
            $obj = new Usuario($usuario["id"],$usuario["username"] ,$usuario["password"],$usuario["email"],$usuario["genero"],$usuario["lat"],$usuario["lon"],$radio,$usuario["estado"],$usuario["descripcion"],
            $usuario["ciudad"],$usuario["edad"],$rangoEdad,$usuario["imgProfile"]);
          }
            $ros = $obj->editarMe();

            print json_encode($res);

        }


        public function buscarNombreInvitado(){
    //este idU->idUsuario hace referencia al invitado del chat
    $match = new Usuario($_POST["idU"],null,null);
    $r = $match->buscarNombreInvitado();
    echo json_encode($r);
  }


    public function hacerFotoPerfil(){
      $i = $_POST["imgProfile"];
      $imgProfile = substr($i, 16);
      $usr = new Usuario(0,$_SESSION['persona'] ,$_SESSION['contraseña']);
      $res = $usr->cargarPerfil();
      foreach($res as $usuario){
        $obj = new Usuario($usuario["id"],$usuario["username"] ,$usuario["password"],$usuario["email"],$usuario["genero"],$usuario["lat"],$usuario["lon"],$usuario["radio"],$usuario["estado"],$usuario["descripcion"],
        $usuario["ciudad"],$usuario["edad"],$usuario["rangoEdad"],$imgProfile);
      }
        $ros = $obj->editarMe();


        print json_encode($ros);

    }




    public function tranquilos(){
        echo "Calmados pues";
    }
}
