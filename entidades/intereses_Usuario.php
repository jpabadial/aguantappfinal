<?php
// include("./libs/DataBase.php");
class intereses_Usuario {
    private $idUsuario;
    private $idInteres;

    protected static $table = "Intereses_Usuario";

    function __construct($idUsuario=null, $idInteres=null) {
        $this->idUsuario = $idUsuario;
        $this->idInteres = $idInteres;
    }

    function getIdUsuario() {
        return $this->idUsuario;
    }

    function getIdInteres() {
        return $this->idInteres;
    }

    function setIdUsuario($idUsuario) {
        $this->idUsuario = $idUsuario;
    }

    function setIdInteres($idInteres) {
        $this->idInteres = $idInteres;
    }

    public function guardarInteres(){

        $db = new DataBase(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        //$db = new DataBase("localhost", "root", "", "aguantapp");
        $values = get_object_vars($this);
        $resultado = $db->insert(self::$table, $values);
        return $resultado;

    }

    public function cambiarInteres(){
      //$db = new DataBase("localhost", "root", "", "aguantapp");
      $db = new DataBase(DB_HOST, DB_USER, DB_PASS, DB_NAME);
      $values = get_object_vars($this);
      $resultado = $db->update(self::$table,$values,"idUsuario = '".$this->getIdUsuario()."'");

      return $resultado;
    }

     public function traerInterest(){
       $db = new DataBase("localhost", "root", "", "aguantapp");
       $resultado = $db->select("idInteres",self::$table,"idUsuario = '".$this->getIdUsuario()."'");

       return $resultado;
     }


}






?>
