<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Categoria
 *
 * @author pabhoz
 */
class Genero {
    private $id;
    private $nombre;
    
    protected static $table = "Genero";
    
    function __construct($id, $nombre) {
        $this->id = $id;
        $this->nombre = $nombre;
    }
    
    function getId() {
        return $this->id;
    }

    function getNombre() {
        return $this->nombre;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }
    
    static function getAll(){
        $db = new DataBase(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        return $db->select("*", self::$table);
    }


}
