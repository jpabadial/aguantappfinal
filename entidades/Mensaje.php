<?php
include("./libs/DataBase.php");

class Mensaje {

    private $id;
    private $owner;
    private $texto;
    private $idChatUno;
    private $idChatOtro;

    protected static $table = "Mensaje";

    function __construct($id,$owner,$texto,$idChatUno,$idChatOtro) {
        $this->id = $id;
        $this->owner = $owner;
        $this->texto = $texto;
        $this->idChatUno = $idChatUno;
        $this->idChatOtro = $idChatOtro;
    }


    function getId() {
        return $this->id;
    }

    function getOwner() {
        return $this->owner;
    }

    function getTexto() {
        return $this->texto;
    }

    function getIdChatUno() {
        return $this->idChatUno;
    }

    function getIdChatOtro() {
        return $this->idChatOtro;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setOwner($owner) {
        $this->owner = $owner;
    }

    function setTexto($texto) {
        $this->texto = $texto;
    }

    function setIdChatUno($idChatUno) {
        $this->idChatUno = $idChatUno;
    }

    function setIdChatOtro($idChatOtro) {
        $this->idChatOtro = $idChatOtro;
    }

    public function enviarMensaje(){
        $db = new DataBase(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        $values = get_object_vars($this);
        $resultado = $db->insert(self::$table, $values);
        return $resultado;
    }

    function cargarMensajesAntiguos(){
      $db = new DataBase(DB_HOST, DB_USER, DB_PASS, DB_NAME);
      return $db->select("owner,texto", self::$table,"idChatUno = '".$this->getIdChatUno()."' AND"
              . " idChatOtro = '".$this->getIdChatOtro()."'OR". " idChatOtro = '".$this->getIdChatUno()."'AND". " idChatUno = '".$this->getIdChatOtro()."'");
    }

      public function eliminarMensajes(){
        $db = new DataBase(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        $values = get_object_vars($this);
        $resultado = $db->delete(self::$table,"idChatUno = '".$this->getIdChatUno()."' AND"
                . " idChatOtro = '".$this->getIdChatOtro()."'OR". " idChatUno = '".$this->getIdChatOtro()."'AND". " idChatOtro = '".$this->getIdChatUno()."'",true);
        return $resultado;

      }

}



?>
